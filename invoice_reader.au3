#include-once
#include <..\..\libraries\autoit_standard_libraries\Utils.au3>
#include <..\..\libraries\autoit_standard_libraries\Excel_file_generation.au3>
#include <..\..\libraries\odczyt_faktur_autoit_libraries\odczyt_faktur_autoit_libraries.au3>

Local $invoicesDirPath
Local $iaListaSzablonow
Local $sTest, $sFakturaTXT
Local $PathPlikTXT, $sKomunikat
Local $iaListaPlikow ;licznik iterujacy po tablicy zawierającej spis faktur PDF
Local $sError, $sExt
Local $sData = @YEAR & "-" & @MON & "-" & @MDAY & "-" & @HOUR & "_" & @MIN & "_" & @SEC & "_"

Local $firstStageDir = "1-WSZYSTKIE_FAKTURY"
Local $secondStageDir = "2-OBROBKA"
Local $thirdStageDir = "3-WYNIKI"
Local $fourthStageDir = "4-ODRZUCONE"

$Main_G_GUI = False	; skrypt NIE wymaga GUI

Func _readFilesIntoArray()
	_Main_Logger("Odczyt plików z folderu: " & $invoicesDirPath & $firstStageDir & "\", "b")
	$aListaPlikow = _FileListToArrayRec ($invoicesDirPath & $firstStageDir, "*.pdf" , $FLTAR_FILES , $FLTAR_NORECUR, $FLTAR_NOSORT, $FLTAR_NOPATH)
	If NOT IsArray($aListaPlikow) Then
		_Main_Logger("Brak plików w folderze: " & $invoicesDirPath & $firstStageDir & "\ - nastąpi zakończenie skryptu.")
		_Main_EndOK("Brak plików w folderze: " & $invoicesDirPath & $firstStageDir & "\")
	EndIf
	_Main_Logger("Znaleziono pliki:" & @CRLF & _Utils_ArrayToRichString($aListaPlikow))
EndFunc

Func _moveFileToNextDir($nazwaPliku)
	_Main_Logger("Przenoszenie pliku z folderu: " & $invoicesDirPath & $firstStageDir & "\ do folderu: " & $invoicesDirPath & $secondStageDir & "\", "b")
	FileMove($invoicesDirPath & $firstStageDir & "\" & $nazwaPliku, $invoicesDirPath & $secondStageDir & "\" & $sData & $nazwaPliku)
	_Main_Logger($nazwaPliku & " --> " & $sData & $nazwaPliku)
EndFunc

Func _PDFtoTXT($nazwaPliku)
	_Main_Logger('Konwersja pliku PDF -> TXT', "b")

	If NOT FileExists($invoicesDirPath & $secondStageDir & "\" & $sData & $nazwaPliku) Then
		_Main_Logger("Brak pliku " & $sData & $nazwaPliku & " w folderze: "  & $invoicesDirPath & $secondStageDir& " - nastąpi zakończenie skryptu.")
		_Main_EndOK("Brak pliku " & $sData & $nazwaPliku & " w folderze: " & $invoicesDirPath & $secondStageDir)
	EndIf

	;konwersja PDT -> TXT
	_Main_Logger("Rozpoczynam konwersję pliku: " & $sData & $nazwaPliku, "b")
	$PathPlikTXT = _o_f_a_l_PDFtoTXT($invoicesDirPath & $secondStageDir & "\" & $sData & $nazwaPliku)
	$sError = @error
	$sExt = @extended
	If $sError = 1 Then
		_Main_Logger("BŁĄD! [ODCZYT FAKTUR][_PDFtoTXT()] zwróciło błąd: " & $PathPlikTXT, "b")
	Else
		_Main_Logger("Wykonano konwersję PDF -> TXT i utworzono plik: " & $PathPlikTXT)
	EndIf
EndFunc

Func _fileVerification($nazwaPliku)
	Local $nazwaPlikuTXT = StringTrimRight($sData & $nazwaPliku, 3) & "txt"
	_Main_Logger('Weryfikacja pliku TXT', "b")
	If NOT FileExists($invoicesDirPath & $secondStageDir & "\" & $nazwaPlikuTXT) Then
		_Main_Logger("Brak pliku " & $nazwaPlikuTXT & " w folderze: C:\ODCZYT_FAKTUR\" &$secondStageDir& " - nastąpi zakończenie skryptu.")
		_Main_EndOK("Brak pliku " & $nazwaPlikuTXT & " w folderze: C:\ODCZYT_FAKTUR\" &$secondStageDir)
	EndIf

	$sFakturaTXT = FileRead($invoicesDirPath & $secondStageDir & "\" & $nazwaPlikuTXT)
	If StringLen($sFakturaTXT) < 10 Then
		FileMove($invoicesDirPath & $secondStageDir & "\" & $sData & $nazwaPliku, $invoicesDirPath & $fourthStageDir & "\" & $sData & $nazwaPliku)
		FileMove($invoicesDirPath & $secondStageDir & "\" & $nazwaPlikuTXT, $invoicesDirPath & $fourthStageDir & "\" & $nazwaPlikuTXT)
		_Main_Logger('Plik: ' & $sData & $nazwaPliku & ' Nie zawierał danych - NAJPRAWDOPODOBNIEJ faktura typu "obrazek".' & @CRLF & "Plik txt i PDF zostały przeniesione do: " & $fourthStageDir, "b")
	EndIf
EndFunc


Func _readFileData($nazwaPliku)
	Local $nazwaPlikuTXT = StringTrimRight($sData & $nazwaPliku, 3) & "txt"
	_Main_Logger('Odczyt danych z faktury', "b")
	If NOT FileExists($invoicesDirPath & $secondStageDir & "\" & $nazwaPlikuTXT) Then
		_Main_Logger("Brak pliku " & $nazwaPlikuTXT & " w folderze: C:\ODCZYT_FAKTUR\" &$secondStageDir& " - nastąpi zakończenie skryptu.")
		_Main_EndOK("Brak pliku " & $nazwaPlikuTXT & " w folderze: C:\ODCZYT_FAKTUR\" &$secondStageDir)
	EndIf

	$sFakturaTXT = FileRead($invoicesDirPath & $secondStageDir & "\" & $nazwaPlikuTXT)
	_Main_Logger("Plik faktury: " & $nazwaPlikuTXT & " po konwersji do TXT:" & @CRLF & $sFakturaTXT)

	; iterujemy po szablonach
	Local $aListaSzablonow = _o_f_a_l_PobierzListeSzablonow()				; tworzymy liste szablonów zawartych w komponencie [szablony_faktur_autoit_components]
	Local $iaSzablon	; licznik wierszy w tablicy szablonu
	Local $bZidentyfikowano = True	; flaga identyfikująca dopasowanie szablonu do pliku TXT faktury
	Local $sInfoZidentyfikowane = "" ; przechowuje wartości zidentyfikowanych pól
	For $iaListaSzablonow = 1 To $aListaSzablonow[0]
	; dopasowanie szablonu do faktury
		Dim $aSzablon[0][0]
		_FileReadToArray($aListaSzablonow[$iaListaSzablonow], $aSzablon, 0, "║")
		If NOT IsArray($aSzablon) Then
			_Main_Logger("BŁĄD! [ODCZYT FAKTUR][ODCZYT SZABLONU] Nie udało się odczytać szablonu: " & $aListaSzablonow[$iaListaSzablonow] & "POMINIĘTO SZABLON")
			ContinueLoop
		EndIf
		_Main_Logger("Załadowano szablon: " & $aListaSzablonow[$iaListaSzablonow])
		$bZidentyfikowano = True
		$sInfoZidentyfikowane = "Szablon: " & $aListaSzablonow[$iaListaSzablonow] & @CRLF
		;iterujemy po szablonie sprawdzamy czy KLUCZ zawiera fragment "identyfikacja_" i weryfikujemy plik faktury
		For $iaSzablon = 0 To UBound($aSzablon, $UBOUND_ROWS)-1
			If StringInStr($aSzablon[$iaSzablon][0], "identyfikacja_") > 0 Then
				If StringInStr($sFakturaTXT, $aSzablon[$iaSzablon][1]) = 0 Then
					_Main_Logger("W fakturze NIE odnaleziono KLUCZA:[" & $aSzablon[$iaSzablon][0] & "] WARTOŚĆ:[" & $aSzablon[$iaSzablon][1] & "]")
					$bZidentyfikowano = False
					ExitLoop
				Else
					$sInfoZidentyfikowane = $sInfoZidentyfikowane & "Zidentyfikowano KLUCZ:[" & $aSzablon[$iaSzablon][0] & "] WARTOŚĆ:[" & $aSzablon[$iaSzablon][1] & "]" & @CRLF
				EndIf
			EndIf
		Next
		If $bZidentyfikowano = True Then ; nastąpiło trafienie na szablon
		; pobieranie danych z faktury przez dopasowany szablon
			_Main_Logger($sInfoZidentyfikowane & @CRLF & "Wszystkie KLUCZE identyfikacyjne zostały odnalezione w fakturze.")
			; iterujemy po szablonie pobierając dane z faktury
			Local $aWynik[0][2]
			For $iaSzablon = 0 To UBound($aSzablon, $UBOUND_ROWS)-1
				If StringInStr($aSzablon[$iaSzablon][0], "identyfikacja_") = 0 Then
					ReDim $aWynik[UBound($aWynik, $UBOUND_ROWS)+1][2]
					$aWynik[UBound($aWynik, $UBOUND_ROWS)-1][0] = $aSzablon[$iaSzablon][0] ; wstawiamy nazwę KLUCZA
					$aWynik[UBound($aWynik, $UBOUND_ROWS)-1][1] = _o_f_a_l_FindValue(_o_f_a_l_Tylda($aSzablon[$iaSzablon][1]), _o_f_a_l_Tylda($aSzablon[$iaSzablon][2])) ; wstawiamy pobraną wartość
					_Main_Logger("Pobieram wartości dla KLUCZA:[" & $aWynik[UBound($aWynik, $UBOUND_ROWS)-1][0] & "]: WARTOŚĆ:[" & $aWynik[UBound($aWynik, $UBOUND_ROWS)-1][1] & "]")
				EndIf
			Next
			_Main_Logger("Wszystkie pobrane wartości z faktury:" & @CRLF & _Utils_ArrayToRichString($aWynik), "b")

			; generowanie plików wyjściowych

			; tworzymy CSV
			$sPlikEksport = StringTrimRight($nazwaPlikuTXT, 4)

			Local $sWynik = ""
			$sWynik = _o_f_a_l_Create_CSV($aWynik, $sPlikEksport,$invoicesDirPath & $thirdStageDir)
			$sError = @error
			$sExt = @extended
			If $sError = 1 Then
				_Main_Logger("BŁĄD! [ODCZYT FAKTUR][_o_f_a_l_Create_CSV()] zwróciło błąd: " & $sWynik, "b")
			EndIf

			; tworzymy XLS
			$sWynik = ""
			$sWynik = _o_f_a_l_Create_XLS($aWynik, $sPlikEksport,$invoicesDirPath & $thirdStageDir)
			$sError = @error
			$sExt = @extended
			If $sError = 1 Then
				_Main_Logger("BŁĄD! [ODCZYT FAKTUR][_o_f_a_l_Create_XLS()] zwróciło błąd: " & $sWynik, "b")
			EndIf

			; przenosimy odczytane pliki PDF i TXT do folderu 3-WYNIKI
			FileMove($invoicesDirPath & $secondStageDir & "\" & $sData & $nazwaPliku, $invoicesDirPath & $thirdStageDir & "\" & $sData & $nazwaPliku)
			FileMove($invoicesDirPath & $secondStageDir & "\" & $nazwaPlikuTXT, $invoicesDirPath & $thirdStageDir & "\" & $nazwaPlikuTXT)
			ExitLoop ;!!!!!!!!!!!!!!!!!!!
		EndIf
	Next
EndFunc

Func _reportIfNoTemplateToMatch($nazwaPliku)
	Local $nazwaPlikuTXT = StringTrimRight($sData & $nazwaPliku, 3) & "txt"
	If FileExists($invoicesDirPath & $secondStageDir & "\" & $sData & $nazwaPliku) Then
		_Main_Logger("Plik dla którego nie znaleziono szablonów: " & $sData & $nazwaPliku & ".","b")
		FileMove($invoicesDirPath & $secondStageDir & "\" & $sData & $nazwaPliku, $invoicesDirPath & $fourthStageDir & "\" & "BRAK_SZABLONU_" & $sData & $nazwaPliku)
		FileMove($invoicesDirPath & $secondStageDir & "\" & $nazwaPlikuTXT, $invoicesDirPath & $fourthStageDir & "\" & "BRAK_SZABLONU_" & $nazwaPlikuTXT)
	EndIf
EndFunc

Func _moveFileToRejectedDir($nazwaPliku)
	_Main_Logger("Przenoszenie pliku z folderu: " & $invoicesDirPath & $firstStageDir & "\ do folderu: " & $invoicesDirPath & $fourthStageDir & "\", "b")
	FileMove($invoicesDirPath & $firstStageDir & "\" & $nazwaPliku, $invoicesDirPath & $fourthStageDir & "\" & $sData & $nazwaPliku)
	_Main_Logger("Plik o nazwie: " & $nazwaPliku & " został odrzucony poniewaz został juz wcześniej przekonwertowany na własciwy format.","b")
EndFunc
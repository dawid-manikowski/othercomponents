#AutoIt3Wrapper_UseX64=y
#include <..\..\libraries\autoit_standard_libraries\ADO.au3>

Global $oConnection = Null

; #FUNCTION# ====================================================================================================================
; Name ..........: _PSQLconnectionStart
; Description ...: Otawrcie polaczenia z bazą SQL
; Syntax ........: _PSQLconnectionStart()
; Parameters ....: $sConnectionString
; Return values .: None
; Author ........: R.W.
; Example .......: No
; ===============================================================================================================================
Func _PSQLconnectionStart($sConnectionString)
	$oConnection = _ADO_Connection_Create()
	Local $ADO_ERR = _ADO_Connection_OpenConString($oConnection, $sConnectionString)

	If @error Then
		_Main_EndError("Błąd połączenia z bazą.", @ScriptName, "Błąd w działaniu funkcji _PSQLPolaczenieStart. Debug info: @error, @extended, $returnValue: " _
				 & @error & ", " & @extended & ", " & $ADO_ERR & ".", True)
	EndIf
EndFunc   ;==>_PSQLconnectionStart


; #FUNCTION# ====================================================================================================================
; Name ..........: _PSQLconnectionEnd
; Description ...: Zamkniecie połączenia z baza
; Syntax ........: _PSQLconnectionEnd()
; Parameters ....: None
; Return values .: None
; Author ........: R.W.
; Example .......: No
; ===============================================================================================================================
Func _PSQLconnectionEnd()
	_ADO_Connection_Close($oConnection)
	$oConnection = Null
EndFunc   ;==>_PSQLconnectionEnd


; #FUNCTION# ====================================================================================================================
; Name ..........: _PSQLinsert
; Description ...: Dodanie nowego wpisu do bazy z nazwa faktury
; Syntax ........: _PSQLinsert()
; Parameters ....: $serviceID, $orderID, $invoiceName
; Return values .: None
; Author ........: R.W.
; Example .......: No
; ===============================================================================================================================
Func _PSQLinsertNewInvoice($serviceID, $orderID, $invoiceName)

	Local $nowTime = @YEAR & "-" & @MON & "-" & @MDAY & " " & (@HOUR-2) & ":" & @MIN & ":" & @SEC

	Local $ADO_ERR = _ADO_Execute($oConnection, "insert into invoice_analyzer (service_id, order_id, invoice_name, reusable, date, status) VALUES ('" _
			 & $serviceID & "','" & $orderID & "','" & $invoiceName & "','TRUE','" & $nowTime & "','NEW');")

	If @error Then _Main_EndError("Blad podczas wykonywania funkcji _PSQLinsertNewInvoice. Debug info: @error, @extended, $returnValue: " & @error & ", " & @extended & ", " & $ADO_ERR & ".")
EndFunc   ;==>_PSQLinsertNewInvoice

; #FUNCTION# ====================================================================================================================
; Name ..........: _PSQLinsertTimeRecord
; Description ...: Dodanie nowego wpisu z czasem trwania bota oraz ilościa faktur
; Syntax ........: _PSQLinsertTimeRecord()
; Parameters ....: $orderid, $nrOfInvoices, $timebot
; Return values .: None
; Author ........: R.W.
; Example .......: No
; ===============================================================================================================================
Func _PSQLinsertTimeRecord($orderID, $nrOfInvoices, $timebot)
	Local $ADO_ERR = _ADO_Execute($oConnection, "insert into invoice_analyzer (order_id, bot_info, bot_time, reusable) VALUES ('" & $orderID & "','" & $nrOfInvoices & "','" & $timebot & "','TRUE');")
	If @error Then _Main_EndError("Blad podczas wykonywania funkcji _PSQLinsertTimeRecord Debug info: @error, @extended, $returnValue: " & @error & ", " & @extended & ", " & $ADO_ERR & ".")
EndFunc   ;==>_PSQLinsertTimeRecord


; #FUNCTION# ====================================================================================================================
; Name ..........: _PSQLcheckIfNew
; Description ...: Sprawdzenie czy faktura jest nowa w bazie danych
; Syntax ........: _PSQLcheckIfNew($invoiceName)
; Parameters ....: $invoiceName
; Return values .: true/false
; Author ........: R.W.
; Example .......: No
; ===============================================================================================================================
Func _PSQLcheckIfNew($invoiceName)
	Local $aRecordset = _ADO_Execute($oConnection, "select * from invoice_analyzer where invoice_name = '" & $invoiceName & "' and reusable = 'true';", True, True)

	If @error <> 13 And @error <> 0 Then
		_Main_EndError("Blad podczas wykonywania funkcji _PSQLcheckIfNew. Debug info: @error, @extended: " & @error & ", " & @extended & ".")
	ElseIf @error = 13 And IsArray($aRecordset) = 0 Then
		Return True
	Else
		Return False
	EndIf
EndFunc   ;==>_PSQLcheckIfNew


; #FUNCTION# ====================================================================================================================
; Name ..........: _PSQLupdateBotId
; Description ...: Funkcja aktualizuje nazwe bota (jego order ID) jeśli wiersz z odpowiednia nazwa pliku jest pusta.
; Syntax ........: _PSQLupdateBotId($fileName, $orderID)
; Parameters ....: $fileName, $orderID - nazwa pliku, order_id skryptu single invoice analyzer
; Return values .: true/false
; Author ........: R.W.
; ===============================================================================================================================
Func _PSQLupdateBotId($fileName, $orderID)
	_ADO_Execute($oConnection, "update invoice_analyzer set bot_id = '" & $orderID & "' where invoice_name = '" & $fileName & "' and bot_id is null;")

	If @error <> 0 Then
		_Main_EndError("Blad podczas wykonywania funkcji _PSQLupdateBotId Debug info: @error, @extended: " & @error & ", " & @extended & ".")
	ElseIf @error = 13 Then
		_Main_EndError("Faktura: " & $fileName & " nie znajduje się w bazie danych. Przypadek do sprawdzenia. _PSQLupdateBotId")
		Return False
	Else
		Return True
	EndIf
EndFunc   ;==>_PSQLupdateBotId

; #FUNCTION# ====================================================================================================================
; Name ..........: _PSQLupdateStatus
; Description ...: Funkcja aktualizuje status bota (NEW -> DONE).
; Syntax ........: _PSQLupdateStatus($fileName, $orderID)
; Parameters ....: $fileName, $orderID - nazwa pliku, order_id skryptu single invoice analyzer
; Return values .: true/false
; Author ........: R.W.
; ===============================================================================================================================
Func _PSQLupdateStatus($fileName, $orderID)
	_ADO_Execute($oConnection, "update invoice_analyzer set status = 'DONE' where invoice_name = '" & $fileName & "' and bot_id = '" & $orderID & "';")

	If @error <> 0 Then
		_Main_EndError("Blad podczas wykonywania funkcji _PSQLupdateStatus Debug info: @error, @extended: " & @error & ", " & @extended & ".")
	ElseIf @error = 13 Then
		_Main_EndError("Faktura: " & $fileName & " nie znajduje się w bazie danych. Przypadek do sprawdzenia. _PSQLupdateStatus")
		Return False
	Else
		Return True
	EndIf
EndFunc   ;==>_PSQLupdateStatus

; #FUNCTION# ====================================================================================================================
; Name ..........: _PSQLcheckIfReusable
; Description ...:	Funkcja sprawdza czy faktura została przetworzona juz przez innego bota.
; Syntax ........: _PSQLcheckIfReusable($invoiceName)
; Parameters ....: $invoiceName
; Return values .: true/false
; Author ........: R.W.
; Example .......: No
; ===============================================================================================================================
Func _PSQLcheckIfReusable($invoiceName, $orderID)
	Local $aRecordset = _ADO_Execute($oConnection, "select reusable from invoice_analyzer where invoice_name = '" & $invoiceName & "' and bot_id = '" & $orderID & "';", True, True)

	If @error <> 13 And @error <> 0 Then
		_Main_EndError("Blad podczas wykonywania funkcji _PSQLcheckIfReusable Debug info: @error, @extended: " & @error & ", " & @extended & ".")
	ElseIf _ArraySearch($aRecordset, "1", 0, 0, 0, 0, 0, False) > 0 Then
		Return True
	Else
		Return False
	EndIf
EndFunc   ;==>_PSQLcheckIfReusable


; #FUNCTION# ====================================================================================================================
; Name ..........: _PSQLinvoiceTimestamp
; Description ...:	Funkcja dodaje timestamp na początku lub końcu procesowania faktury
; Syntax ........: _PSQLinvoiceTimestamp($fileName, $start)
; Parameters ....:
; Return values .:
; Author ........: R.W.
; Example .......: No
; ===============================================================================================================================
Func _PSQLinvoiceTimestamp($fileName, $start)

	Local $nowTime = @YEAR & "-" & @MON & "-" & @MDAY & " " & (@HOUR-2) & ":" & @MIN & ":" & @SEC

	If $start = True Then
		_ADO_Execute($oConnection, "update invoice_analyzer set invoice_timer_start = '" & $nowTime & "' where invoice_name = '" & $fileName & "';")
	Else
		_ADO_Execute($oConnection, "update invoice_analyzer set invoice_timer_end = '" & $nowTime & "' where invoice_name = '" & $fileName & "';")
	EndIf

	If @error <> 0 Then
		_Main_EndError("Blad podczas wykonywania funkcji _PSQLinvoiceTimestamp Debug info: @error, @extended: " & @error & ", " & @extended & ".")
	ElseIf @error = 13 Then
		_Main_EndError("Faktura: " & $fileName & " nie znajduje się w bazie danych. Przypadek do sprawdzenia. _PSQLinvoiceTimestamp")
		Return False
	Else
		Return True
	EndIf
EndFunc


; #FUNCTION# ====================================================================================================================
; Name ..........: _PSQLupdateReusable
; Description ...:	Funkcja aktualizuje kolumnę dp_reusable i ustawia wartość false
; Syntax ........: _PSQLupdateReusable($invoiceName)
; Parameters ....: $invoiceName
; Return values .: true/false
; Author ........: R.W.
; Example .......: No
; ===============================================================================================================================
Func _PSQLupdateReusable($fileName)
	_ADO_Execute($oConnection, "update invoice_analyzer set reusable = 'false' where invoice_name = '" & $fileName & "' and reusable = 'true';")

	If @error <> 0 Then
		_Main_EndError("Blad podczas wykonywania funkcji _PSQLupdateReusable Debug info: @error, @extended: " & @error & ", " & @extended & ".")
	ElseIf @error = 13 Then
		_Main_EndError("Faktura: " & $fileName & " nie znajduje się w bazie danych. Przypadek do sprawdzenia. _PSQLupdateReusable")
		Return False
	Else
		Return True
	EndIf
EndFunc   ;==>_PSQLupdateReusable


; #FUNCTION# ====================================================================================================================
; Name ..........: _PSQLresetTable
; Description ...:	Funkcja usuwa wszystkie wpisy z tabeli oraz resetuje klucz główny ID tabeli do stanu początkowego
; Syntax ........: _PSQLresetTable()
; Parameters ....: None
; Return values .: None
; Author ........: R.W.
; Example .......: No
; ===============================================================================================================================
Func _PSQLresetTable()
	Local $ADO_ERR = _ADO_Execute($oConnection, "truncate invoice_analyzer; ALTER SEQUENCE invoice_analyzer_id_seq RESTART WITH 1;")
	If @error Then _Main_EndError("Blad podczas wykonywania funkcji _PSQLresetTable. Debug info: @error, @extended, $returnValue: " & @error & ", " & @extended & ", " & $ADO_ERR & ".")
EndFunc   ;==>_PSQLresetTable

; #FUNCTION# ====================================================================================================================
; Name ..........: _PSQLgetbotInfoAndTime
; Description ...:	Funkcja zwraca wartości z kolumnt bot_time oraz bot_info
; Syntax ........: _PSQLgetbotInfoAndTime()
; Parameters ....: None
; Return values .: $aRecordset
; Author ........: R.W.
; Example .......: No
; ===============================================================================================================================
Func _PSQLgetbotInfoAndTime()
	Local $aRecordset = _ADO_Execute($oConnection, "Select * from invoice_analyzer where bot_time is not null;", True, True)
	If @error Then _Main_EndError("Blad podczas wykonywania funkcji _PSQLgetbotInfoAndTime. Debug info: @error, @extended, $returnValue: " & @error & ", " & @extended & ".")
	Return $aRecordset
EndFunc   ;==>_PSQLgetbotInfoAndTime

#Region for testing purpose
Func _PSQLgetTable()
	Local $aRecordset = _PSQLAll()
	_ShowResult($aRecordset, "invoice_analyzer")
EndFunc   ;==>_PSQLgetTable

Func _PSQLAll()
	Local $aRecordset = _ADO_Execute($oConnection, "Select * from invoice_analyzer", True, True)
	Return $aRecordset
EndFunc   ;==>_PSQLAll

Func _ShowResult(ByRef $aRecordset, $Komentarz)
	If IsArray($aRecordset) Then
		_ArrayDisplay($aRecordset, $Komentarz)
	Else
		MsgBox(0, "Wynik query = string", $aRecordset)
	EndIf
EndFunc   ;==>_ShowResult
#EndRegion for testing purpose
